﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WebApi_DhrDwh2Sf.Models;
using WebApi_DhrDwh2Sf.Controllers;
using System.Data;
using WebApi_DhrDwh2Sf.WebServiceConsumer;
using Newtonsoft.Json;


namespace DhrDwf2Sf_UnitTest
{
    /// <summary>
    /// Summary description for CarrierRatesControllerTests
    /// </summary>
    [TestClass]
    public class CarrierControllerTests
    {

        CarrierController carrierController;

        public CarrierControllerTests()
        {
            //
            // TODO: Add constructor logic here
            //

            this.carrierController = new CarrierController();
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext) { }
        
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML", "CarrierContoller_TestsDataSource.xml", "GetAll", DataAccessMethod.Sequential)]
        public void CarrierRates_GetAll()
        {
            var expectedStatus = (string)TestContext.DataRow["ExpectedStatus"];
            Assert.AreEqual(this.carrierController.GetAll().Status, expectedStatus.ToString());
        }
    }
}
