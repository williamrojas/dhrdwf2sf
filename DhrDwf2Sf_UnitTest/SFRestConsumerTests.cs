﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WebApi_DhrDwh2Sf.WebServiceConsumer;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using Newtonsoft.Json;
using System.Data.Linq;
using System.Linq;

namespace DhrDwf2Sf_UnitTest
{
    [TestClass]
    public class SFRestConsumerUnitTests
    {
        public SFRestConsumerUnitTests()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }




        //[TestMethod]
        public void SendNotification_Successfull()
        {
            SFRestConsumer c = new SFRestConsumer();
            DHRRequestContent r = new DHRRequestContent()
            {
                Status = DHRRequestContent.ResponseStatus.Ok.ToString(),
                Content = new List<string>(){"The file processed successfully", "Filename: test filename" },
                Count=1,
                Id = "a108A000000yKK8"
            };
            var response = c.PostRequest(r, SFRestConsumer.EndPoint.DHRNotification);
            Assert.AreEqual(System.Net.HttpStatusCode.OK, response.StatusCode);

            DHRRequestContent res = JsonConvert.DeserializeObject<DHRRequestContent>(response.Content.ReadAsStringAsync().Result, new JsonSerializerSettings(){NullValueHandling = NullValueHandling.Ignore});
            Assert.AreEqual(DHRRequestContent.ResponseStatus.Ok.ToString(), res.Status);
        }

        //[TestMethod]
        public void SendNotification_Fail()
        {
            SFRestConsumer c = new SFRestConsumer();
            DHRRequestContent r = new DHRRequestContent()
            {
                Status = DHRRequestContent.ResponseStatus.Error.ToString(),
                Content = new List<string>() { "The file failed to process ", "Invalid State codes" },
                Count = 1
                //,Id = "a108A000000yKK8"
            };

            var response = c.PostRequest(r, SFRestConsumer.EndPoint.DHRNotification);
            Assert.AreEqual(System.Net.HttpStatusCode.InternalServerError, response.StatusCode);
        }


        //[TestMethod]
        public void DecryptString()
        {
            var str = "0B07A4D7FC5E70CEBA02B88CEB7BB2253C9E3ED7986506D7E3D2E5AB14BFAA15";
            var bytes = string.Join(string.Empty, str.Select((x, i) => i > 0 && i % 2 == 0 ? string.Format(",{0}", x) : x.ToString())).Split(',').Select(s => Convert.ToByte(s, 16)).ToArray();
            var sDecrypted = new BIG.ConfigHelper.Encryptor(Encoding.Unicode).DecryptStringFromBytes(bytes, "DHR", "BANKERSCORP");

            Assert.AreEqual(sDecrypted, "William:Rojas");
        }

        //[TestMethod]
        public void EncryptString()
        {
            System.Text.Encoding enc = Encoding.Unicode;

            string originalVal = "{'grant_type':'password','client_id':'3MVG9eQyYZ1h89HfMJhaXQwBFKP3OYz8GbviIUOJawtk2Ii1B2Hrz5RpfppBkzSRfon3CyO7_Xkwd315wrj5N','client_secret':'4227493062187600206','username':'william.rojas@bankersfinancialcorp.com.fullsb','password':'wRojas12345e01wzLExcA5pYxuOZMnLWbKsG'}";
            
            //get the encrypted bytes
            var bytes = new BIG.ConfigHelper.Encryptor(enc).EncryptStringToBytes(originalVal, "DHR", "BANKERSCORP");

            //convert it to hex string
            var s2 = string.Join(",", Array.ConvertAll(bytes, b => b.ToString("X2")));

            //make it an array
            bytes = s2.Split(',').Select(s => Convert.ToByte(s, 16)).ToArray();
            
            //decrypt it
            var sDecrypted = new BIG.ConfigHelper.Encryptor(enc).DecryptStringFromBytes(bytes, "DHR", "BANKERSCORP");

            Assert.AreEqual(originalVal, sDecrypted);
        }
    }


}
