﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Routing;

namespace WebApi_DhrDwh2Sf
{
    /// <summary>
    /// Class helper to automtically log the incoming requests
    /// </summary>
    public class ApiLogHandler : DelegatingHandler
    {
        /// <summary>
        /// Intersects the request and logs its attributes
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        //protected async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            
            var apiLogEntry = CreateApiLogEntryWithRequestData(request);
            if (request.Content != null)
            {
                await request.Content.ReadAsStringAsync()
                    .ContinueWith(task =>
                    {
                        apiLogEntry.RequestContentBody = task.Result;
                    }, cancellationToken);
            }

            return await base.SendAsync(request, cancellationToken)
                .ContinueWith(task =>
                {
                    var response = task.Result;
                    try
                    {

                        // Update the API log entry with response info
                        apiLogEntry.ResponseStatusCode = (int)response.StatusCode;
                        apiLogEntry.ResponseTimestamp = DateTime.Now;

                        if (!(response.Content == null || apiLogEntry.RequestRouteTemplate.StartsWith("swag")))
                        {
                            //apiLogEntry.ResponseContentBody = response.Content.ReadAsStringAsync().Result;
                            apiLogEntry.ResponseContentBody = JsonConvert.DeserializeObject(response.Content.ReadAsStringAsync().Result);
                            //apiLogEntry.ResponseContentBody = JsonConvert.DeserializeObject(response.Content.ReadAsStringAsync().Result,
                            //                                    new JsonSerializerSettings(){ ReferenceLoopHandling = ReferenceLoopHandling.Ignore});

                            apiLogEntry.ResponseContentType = response.Content.Headers.ContentType.MediaType;
                            //apiLogEntry.ResponseHeaders = SerializeHeaders(response.Content.Headers);
                            apiLogEntry.ResponseHeaders = response.Content.Headers;
                        }


                        Logger.log.Info("Request received:" + JsonConvert.SerializeObject(apiLogEntry));
                    }
                    catch (Exception)
                    {
                        //throw;
                    }

                    return response;
                }, cancellationToken);
        }

        /// <summary>
        /// Creates/Parses the request into an ApiLogEntry
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ApiLogEntry CreateApiLogEntryWithRequestData(HttpRequestMessage request)
        {
            var context = ((HttpContextBase)request.Properties["MS_HttpContext"]);
            var routeData = request.GetRouteData();

            return new ApiLogEntry
            {
                //Application = "[insert-calling-app-here]",
                User = context.User.Identity.Name,
                Machine = Environment.MachineName,
                RequestContentType = context.Request.ContentType,
                RequestRouteTemplate = routeData.Route.RouteTemplate,
                //RequestRouteData = SerializeRouteData(routeData),
                RequestRouteData = routeData,
                RequestIpAddress = context.Request.UserHostAddress,
                RequestMethod = request.Method.Method,
                //RequestHeaders = SerializeHeaders(request.Headers),
                RequestHeaders = request.Headers,
                RequestTimestamp = DateTime.Now,
                RequestUri = request.RequestUri.ToString()
            };
        }

    }
}