﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi_DhrDwh2Sf
{
    /// <summary>
    /// A file being sent thru a web service
    /// </summary>
    public class WCRateFile
    {
        /// <summary>The file content enconded as a Base64 string blob</summary>
        public string EncodedBlob { get; set; }

        /// <summary>The file name</summary>
        public string FileName { get; set; }
    }
}