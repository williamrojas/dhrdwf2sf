﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace WebApi_DhrDwh2Sf.Authentication
{
    //System.Web.Http.Filters.ActionFilterAttribute class as shown below:

    /// <summary>
    /// Overloads the implementation of the ActionFilterAttribute to enable basic authentication for the web methods
    /// </summary>
    public class BasicAuthenticationAttribute : System.Web.Http.Filters.ActionFilterAttribute
    {
        private AppSettings settings = new AppSettings();

        private Boolean IsUserValid(Dictionary<string, string> credentials)
        {
            if (credentials["UserName"].Equals("joydip") && credentials["Password"].Equals("joydip123"))
                return true;

            return false;
        }

        private Dictionary<string, string> ParseRequestHeaders(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            Dictionary<string, string> credentials = new Dictionary<string, string>();

            var httpRequestHeader = actionContext.Request.Headers.GetValues("Authorization").FirstOrDefault();
            httpRequestHeader = httpRequestHeader.Substring("Authorization".Length);

            string[] httpRequestHeaderValues = httpRequestHeader.Split(':');
            string username = Encoding.UTF8.GetString(Convert.FromBase64String(httpRequestHeaderValues[0]));
            string password = Encoding.UTF8.GetString(Convert.FromBase64String(httpRequestHeaderValues[1]));

            credentials.Add("UserName", username);
            credentials.Add("Password", password);

            return credentials;
        }


        /// <summary>
        /// Validates the credentials sent by the third consumer app
        /// </summary>
        /// <param name="encryptedCredentials">
        /// The encrypted credentials.
        /// <returns></returns>
        private bool ValidateCredentials(string encryptedCredentials)
        {

            //Note: Since there is only one user at this point, the authentication will be stored in the config file
            //This can be changed so that username and paswords are retrieved/validated from the database

            //string decryptedCredentials = BIG.ConfigHelper.Extensions.DecryptStringFromBytes(actionContext.Request.Headers.Authorization.Parameter,
            //                                                                                this.settings.ApiAuthorizationPassword, this.settings.ApiAuthorizationSalt).split(':');
            //string username = decryptedCredentials[0];
            //string password = decryptedCredentials[1];

            //if needed, write code here to validate against a database

            return (encryptedCredentials != this.settings.ApiAuthorizationString);
        }
        
        /// <summary>
        /// OVerride method to manually validate the users
        /// </summary>
        /// <param name="actionContext"></param>
        public override void  OnActionExecuting(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            try
            {
                var apiLogEntry = new ApiLogHandler().CreateApiLogEntryWithRequestData(actionContext.Request);

                //IPAddress addr = IPAddress.Parse(apiLogEntry.RequestIpAddress);
                //IPHostEntry entry = Dns.GetHostEntry(addr);

                if (actionContext.Request.Headers.Authorization == null)
                {
                    actionContext.Response = new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.Unauthorized);
                }
                else
                {
                    if (this.ValidateCredentials(actionContext.Request.Headers.Authorization.Parameter))
                        actionContext.Response = new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.Unauthorized);    
                    //else
                    //   actionContext.Response = new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.OK);
                }
            }
            catch (Exception ex)
            {
                actionContext.Response = new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.InternalServerError);
            }
        }
    }
}