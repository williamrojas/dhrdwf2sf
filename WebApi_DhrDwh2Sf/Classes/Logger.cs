﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi_DhrDwh2Sf
{
    /// <summary>
    /// Publicly accessible log4net logger
    /// </summary>
    public class Logger
    {
        /// <summary>The main log</summary>
        public static readonly log4net.ILog log = log4net.LogManager.GetLogger("");
    }
}