﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
//using WebApi_DhrDwh2Sf.Models;

namespace WebApi_DhrDwh2Sf.WebServiceConsumer
{

    /// <summary>
    /// Creates and consumes an api service from salesforce
    /// </summary>
    public class SFRestConsumer
    {

        AppSettings settrings = new AppSettings();

        /// <summary>Enumeration of the availabe endpoints in Salesforce</summary>
        public enum EndPoint { 
            
            /// <summary>Endpoint to post notifications to</summary>
            DHRNotification
        }

        /// <summary>
        /// Connects to a salesforce instace using the encrypted values saved in the config file
        /// </summary>
        /// <returns></returns>
        public HttpResponseMessage Connect()
        {
            HttpClient authClient = new HttpClient();

            //specify to use TLS 1.2 as default connection
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

            //this.settrings.SFAuthorizationSettings.Split(',').Select(s => Convert.ToByte(s, 16)).ToArray(),
            HttpContent content = new FormUrlEncodedContent(
                JsonConvert.DeserializeObject<Dictionary<string, string>>
                (
                    new BIG.ConfigHelper.Encryptor(System.Text.Encoding.Unicode).DecryptStringFromBytes(
                            string.Join(string.Empty, this.settrings.SFAuthorizationSettings.Select((x, i) => i > 0 && i % 2 == 0 ? string.Format(",{0}", x) : x.ToString())).Split(',').Select(s => Convert.ToByte(s, 16)).ToArray(),
                            this.settrings.ApiAuthorizationPassword, 
                            this.settrings.ApiAuthorizationSalt)
                
                
                ));

            var message = authClient.PostAsync(this.settrings.SFAuthorizationURL, content).Result;
            return message;
        }

        /// <summary>
        /// Connects to a salesforce instace
        /// </summary>
        /// <param name="sfdcConsumerKey">The consumer key (provided by SF)</param>
        /// <param name="sfdcConsumerSecret">The consumer secret (provided by SF)</param>
        /// <param name="sfdcUserName">Salesforce username</param>
        /// <param name="sfdcPassword">Password username</param>
        /// <param name="sfdcToken">The token for the user (provided by SF)</param>
        /// <returns></returns>
        public HttpResponseMessage Connect(string sfdcConsumerKey, string sfdcConsumerSecret, string sfdcUserName, string sfdcPassword, string sfdcToken)
        {

            HttpClient authClient = new HttpClient();

            //specify to use TLS 1.2 as default connection
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

            //create login password value
            string loginPassword = sfdcPassword + sfdcToken;
            //string loginPassword = sfdcPassword;

            HttpContent content = new FormUrlEncodedContent(new Dictionary<string, string>
                {
                    {"grant_type","password"},
                    {"client_id",sfdcConsumerKey},
                    {"client_secret",sfdcConsumerSecret},
                    {"username",sfdcUserName},
                    {"password",loginPassword}
                });
            
            var message = authClient.PostAsync(this.settrings.SFAuthorizationURL, content).Result;
            return message;
        }


        /// <summary>
        /// Generates and consumes a post request on salesforce
        /// </summary>
        /// <param name="dhrRequest">The content as a DHRRequestContent</param>
        /// <param name="endpoint">The partial endpoint as specified by the enumerator</param>
        /// <returns>The httpresponse as returned from the endpoint</returns>
        public HttpResponseMessage PostRequest(DHRRequestContent dhrRequest, SFRestConsumer.EndPoint endpoint)
        {
            return this.PostRequest(dhrRequest, endpoint.ToString());
        }

        /// <summary>
        /// Generates and consumes a post request on salesforce
        /// </summary>
        /// <param name="dhrRequest">The content as a DHRRequestContent</param>
        /// <param name="endpoint">The partial endpoint</param>
        /// <returns>The httpresponse as returned from the endpoint</returns>
        public HttpResponseMessage PostRequest(DHRRequestContent dhrRequest, string endpoint)
        {
            HttpResponseMessage httpMessage = this.Connect();

            if (httpMessage.StatusCode == System.Net.HttpStatusCode.BadRequest)
                throw new Exception();

            var responseString = httpMessage.Content.ReadAsStringAsync().Result;
            Dictionary<string, string> responseDict = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, string>>(responseString);
            //object obj = Newtonsoft.Json.JsonConvert.DeserializeObject(responseString);

            if (!responseDict.ContainsKey("access_token") && responseDict.ContainsKey("instance_url"))
                throw new Exception();

            var oauthToken = responseDict["access_token"];
            var serviceUrl = responseDict["instance_url"];

            HttpClient queryClient = new HttpClient();

            //QUERY: Retrieve records of type "account"
            //string restQuery = serviceUrl + "/services/data/v25.0/sobjects/Account";
            //QUERY: retrieve a specific account
            //string restQuery = serviceUrl + "/services/data/v25.0/sobjects/Account/001E000000N1H1O";
            //QUERY: Perform a SELECT operation
            //string restQuery = serviceUrl + "/services/data/v25.0/query?q=SELECT+name+from+Account";


            //string restQuery = serviceUrl + "/services/apexrest/DHRNotification/";
            string restQuery = serviceUrl + "/services/apexrest/" + endpoint + "/";

            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, restQuery);
            request.Content = new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(dhrRequest));
            request.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            //add token to header
            request.Headers.Add("Authorization", "Bearer " + oauthToken);

            //return XML to the caller
            //request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/xml"));
            //return JSON to the caller
            request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            //call endpoint async
            HttpResponseMessage response = queryClient.SendAsync(request).Result;
            
            return response;
        }

    }
}