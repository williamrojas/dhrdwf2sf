﻿using BIG.ConfigHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi_DhrDwh2Sf
{
    /// <summary>
    /// Self configurable class to automatically read the settings from config file
    /// </summary>
    public class AppSettings : BIG.ConfigHelper.SelfConfigurableClass
    {

        [ProcessAttributes(IsSettingInConfigFile = true, RequiredInConfigFile = true)]
        public string WCRatesDropoffPath { get; set; }

        [ProcessAttributes(IsSettingInConfigFile = true, RequiredInConfigFile = true)]
        public string ApiAuthorizationString { get; set; }

        [ProcessAttributes(IsSettingInConfigFile = true, RequiredInConfigFile = true)]
        public string ApiAuthorizationPassword { get; set; }

        [ProcessAttributes(IsSettingInConfigFile = true, RequiredInConfigFile = true)]
        public string ApiAuthorizationSalt { get; set; }

        [ProcessAttributes(IsSettingInConfigFile = true, RequiredInConfigFile = true)]
        public string SFAuthorizationSettings { get; set; }

        [ProcessAttributes(IsSettingInConfigFile = true, RequiredInConfigFile = true)]
        public string SFAuthorizationURL { get; set; }
        
    }
}