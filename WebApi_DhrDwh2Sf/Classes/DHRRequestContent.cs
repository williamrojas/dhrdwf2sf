﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi_DhrDwh2Sf.WebServiceConsumer
{
    /// <summary>
    /// A generic response for any of the api calls
    /// </summary>
    public class DHRRequestContent
    {
        private IEnumerable<Object> _content;

        /// <summary>
        /// The status/result of the transaction
        /// </summary>
        public enum ResponseStatus { Ok=0, Error=1}

        /// <summary>The Id of the originating system (i.e. ID of salesforce record)</summary>
        public String Id { get; set; }


        /// <summary>The status/result of the transaction (as a string)</summary>
        public String Status { get; set; }

        /// <summary>The number of objects in the collection</summary>
        public int Count { get; set; }

        /// <summary>A collection of objects with the reqeusted data</summary>
        public IEnumerable<Object> Content {
            get { return this._content; } 
            set {
                this._content = value;
                if (this.Status == ResponseStatus.Error.ToString())
                    this.Count = 0;
                else
                    this.Count = value.Count();
            } 
        }
    }
}