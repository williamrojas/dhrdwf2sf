﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApi_DhrDwh2Sf.Models;
using WebApi_DhrDwh2Sf.WebServiceConsumer;

namespace WebApi_DhrDwh2Sf.Controllers
{
    public class CarrierController : ApiController
    {

        Models.DHRDataWarehouseDataContext cDb;

        /// <summary>
        /// Gets all the carriers in the database
        /// </summary>
        /// <returns></returns>
        public DHRRequestContent GetAll()
        {
            cDb = new Models.DHRDataWarehouseDataContext();

            var query = (from c in cDb.WCCarriers
                         select new Carrier 
                         {
                            ID= c.ID,
                            Name = c.Name,
                         }).ToList();

            return new DHRRequestContent() { Status = DHRRequestContent.ResponseStatus.Ok.ToString(), Content = query };
        }
    }
}
