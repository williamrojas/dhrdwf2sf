﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
//using WebApi_DhrDwh2Sf.Authentication;
using WebApi_DhrDwh2Sf.WebServiceConsumer;

namespace WebApi_DhrDwh2Sf.Controllers
{
    /// <summary>
    /// Test endpoint
    /// </summary>
    [WebApi_DhrDwh2Sf.Authentication.BasicAuthentication]
    public class TestController : ApiController
    {
        /// <summary>
        /// Test get method
        /// </summary>
        /// <returns>List of two string ("Test1","Test2") encapsulated in a DHRRequestContent</returns>
        public DHRRequestContent Get()
        {
            return new DHRRequestContent() { Status = DHRRequestContent.ResponseStatus.Ok.ToString(), Content = new string[] { "Test1", "Test2"} };
        }
    }
}
