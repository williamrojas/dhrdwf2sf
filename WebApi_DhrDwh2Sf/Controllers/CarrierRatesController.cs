﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Http.Results;
using WebApi_DhrDwh2Sf.Models;
using WebApi_DhrDwh2Sf.WebServiceConsumer;

namespace WebApi_DhrDwh2Sf.Controllers
{
    public class CarrierRatesController : ApiController
    {
        private AppSettings settings = new AppSettings();
        Models.DHRDataWarehouseDataContext cDb;
        

        /// <summary>
        /// Gets all the rates in the database -- not only the latest one
        /// </summary>
        /// <returns></returns>
        public DHRRequestContent GetAll()
        {
            cDb = new Models.DHRDataWarehouseDataContext();

            var query = (from r in cDb.WCRates
                         join s in cDb.WCStates on r.StateID equals s.ID
                         join c in cDb.WCCarriers on s.CarrierID equals c.ID
                         select new CarrierRates
                         {
                             ID = r.ID,
                             ClassCode = r.ClassCode,
                             State = s.State,
                             FinalRate = r.FinalRate,
                             CarrierId = c.ID,
                             StartDate = s.StartDate
                         });

            return new DHRRequestContent() { Status = DHRRequestContent.ResponseStatus.Ok.ToString(), Content = query };
        }

        /// <summary>
        /// Generic method to get the 'CarrierRates' records 
        /// </summary>
        /// <param name="filter"></param>
        /// <returns>A list of CarrierRates</returns>
        private IEnumerable<CarrierRates> GetLastList([FromUri] CarrierRates filter)
        {
            cDb = new Models.DHRDataWarehouseDataContext();
            var query = (from r in cDb.WCRates
                         join s in cDb.WCStates on r.StateID equals s.ID
                         join c in cDb.WCCarriers on s.CarrierID equals c.ID
                         //where s.StartDate <= DateTime.Now
                         select new CarrierRates
                         {
                             ID = r.ID,
                             ClassCode = r.ClassCode,
                             State= s.State,
                             FinalRate = r.FinalRate,
                             CarrierId = c.ID,
                             StartDate = s.StartDate
                         })
                         
                        // .GroupBy(a => a.StartDate)
                        //.OrderByDescending(i => i.Key).First()
                         ;

            if (!(filter.State == null || filter.State == string.Empty))
                query = query.Where(a => a.State == filter.State);

            if (!(filter.ClassCode == null || filter.ClassCode == string.Empty))
                query = query.Where(a => a.ClassCode == filter.ClassCode);

            if (filter.CarrierId != 0)
                query = query.Where(a => a.CarrierId == filter.CarrierId);

            //query = query.GroupBy(a => new { a.State, a.ClassCode, a.CarrierId })
            // .Select(g => g.OrderByDescending(i => i.StartDate).First())
            // .OrderBy(a => a.State).ThenByDescending(a => a.StartDate);

            query = query.GroupBy(a => a.StartDate)
                .OrderByDescending(i => i.Key).First().ToList().AsQueryable();
            ;

            return query;
        }

        /// <summary>
        /// Gets the last available Carrier Rate
        /// </summary>
        /// <param name="filter">
        /// The filter to apply to the query
        /// <para>State: Required</para>
        /// <para>ClassCode: Required</para>
        /// <para>Carrier: Not required</para>
        /// </param>
        /// <returns>A list of CarrierRates</returns>
        public DHRRequestContent GetLast([FromUri] CarrierRates filter)
        {
            if ((filter.State == null || filter.State == string.Empty))
                return new DHRRequestContent() { Status = DHRRequestContent.ResponseStatus.Error.ToString(), Content = new string[] { "Error: No parameter was specified: State" } };
            try
            {
                return new DHRRequestContent() { Status = DHRRequestContent.ResponseStatus.Ok.ToString(), Content = this.GetLastList(filter) };
            }
            catch (Exception ex)
            {
                Logger.log.Error("", ex);
                throw ex;
            }
        }

        /// <summary>
        /// Generic method to get the 'CarrierRates' records 
        /// </summary>
        /// <param name="filter">The filter to apply to the query
        /// <para>State: Not Required</para>
        /// <para>ClassCode: Required</para>
        /// <para>Carrier: Not required</para>
        /// </param>
        /// <returns>A list of CarrierRates</returns>
        public DHRRequestContent GetLastAll([FromUri] CarrierRates filter)
        {   
            return new DHRRequestContent() { Status = DHRRequestContent.ResponseStatus.Ok.ToString(), Content = this.GetLastList(filter) };
        }

        /// <summary>
        /// Saves a WCRateFile to disk
        /// </summary>
        /// <param name="encodedFile">Object with the encoded binary data as base-64 digits and filename
        /// <example><para></para>Ex: {
        /// <para></para>    "encodedBlob" : "MTIzNDU2OTA=",
        /// <para></para>    "filename":"test.txt" 
        /// <para></para>}
        /// </example>
        /// </param>
        [HttpPost]
        public DHRRequestContent ProcessWCRatesFile([FromBody] WCRateFile encodedFile)
        {
            DHRRequestContent wc = null;
            
            try
            {
                var destFile = Path.Combine(this.settings.WCRatesDropoffPath, encodedFile.FileName);
                Byte[] bytes = Convert.FromBase64String(encodedFile.EncodedBlob);
                if (File.Exists(destFile))
                    wc = new DHRRequestContent() { Status = DHRRequestContent.ResponseStatus.Error.ToString(), Content = new string[] { "The file already exists" } };
                else
                {
                    File.WriteAllBytes(destFile, bytes);
                    wc = new DHRRequestContent() { Status = DHRRequestContent.ResponseStatus.Ok.ToString(), Content = new string[] { "The file was received successfully" } };
                }
            }
            catch (Exception ex)
            {
                Logger.log.Error("", ex);
                throw;
            }

            return wc;
        }

        /// <summary>
        /// Sends a notification back to salesforce
        /// <para></para>Note: the notification will be handled on the salesforce side which
        /// <para></para>can be an email, chatter post, etc.
        /// </summary>
        /// <param name="dhrReqContent">The noticiation status and description encoded as a DHRRequestContent</param>
        /// <returns></returns>
        [HttpPost]
        public DHRRequestContent SendNotification([FromBody] DHRRequestContent dhrReqContent)
        {
            DHRRequestContent wc = null;

            try
            {
                var response = new SFRestConsumer().PostRequest(dhrReqContent, SFRestConsumer.EndPoint.DHRNotification);
                wc = Newtonsoft.Json.JsonConvert.DeserializeObject<DHRRequestContent>(
                    response.Content.ReadAsStringAsync().Result, 
                    new JsonSerializerSettings(){NullValueHandling = NullValueHandling.Ignore});
            }
            catch (Exception ex)
            {
                Logger.log.Error("", ex);
                throw;
            }

            return wc;
        }
    }
}

