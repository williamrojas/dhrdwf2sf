﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApi_DhrDwh2Sf.WebServiceConsumer;

namespace WebApi_DhrDwh2Sf.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class SutaRatesController : ApiController
    {
        Models.DHRDataWarehouseDataContext cDb;

        /// <summary>
        /// Gets all the available Suta records
        /// </summary>
        /// <returns><see cref="WebApi_DhrDwh2Sf.WebServiceConsumer.DHRRequestContent"/></returns>
        public DHRRequestContent GetAll()
        {
            try
            {
                cDb = new Models.DHRDataWarehouseDataContext();
                var query = (from r in cDb.SutaRates select r);
                return new DHRRequestContent() { Status = DHRRequestContent.ResponseStatus.Ok.ToString(), Content = query };
            }
            catch (Exception ex)
            {
                Logger.log.Error("", ex);
                throw ex;
            }
        }
        
        /// <summary>
        /// Gets the latest Suta records
        /// </summary>
        /// <param name="states">The States to filter the data (Optional)</param>
        /// <returns></returns>
        private DHRRequestContent GetLastList(String states = null)
        {
            try
            {
                cDb = new Models.DHRDataWarehouseDataContext();
                var query = (from r in cDb.SutaRates select r);

                if (!(states == null || states == string.Empty)) {
                    var l = new List<String>(states.Split(','));
                    //query = query.Where(a => a.State == state);
                    query = query.Where(a => l.Contains(a.State));
                }

                query = query.GroupBy(a => new { a.State })
                            .Select(g => g.OrderByDescending(i => i.Insert_dts).First())
                            .OrderBy(a => a.State);

                return new DHRRequestContent() { Status = DHRRequestContent.ResponseStatus.Ok.ToString(), Content = query };
            }
            catch (Exception ex)
            {
                Logger.log.Error("", ex);
                throw ex;
            }
        }

        /// <summary>
        /// Gets the latest availabel suta rate from the database
        /// </summary>
        /// <param name="States">List of states (as csv)</param>
        /// <returns></returns>
        public DHRRequestContent GetLast(String States)
        {
            if ((States == null || States == string.Empty))
                return new DHRRequestContent() { Status = DHRRequestContent.ResponseStatus.Error.ToString(), Content = new string[] { "Error: No parameter was specified: State" } };

            return this.GetLastList(States);            
        }

        /// <summary>
        /// Gets the latest available suta rate for all the states
        /// </summary>
        /// <returns></returns>
        public DHRRequestContent GetLastAll()
        {
            return this.GetLastList(null);
        }
    }
}
