﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi_DhrDwh2Sf.Models
{
    public class Carrier
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
}