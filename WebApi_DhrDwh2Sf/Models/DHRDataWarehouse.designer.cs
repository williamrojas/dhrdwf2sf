﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34209
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebApi_DhrDwh2Sf.Models
{
	using System.Data.Linq;
	using System.Data.Linq.Mapping;
	using System.Data;
	using System.Collections.Generic;
	using System.Reflection;
	using System.Linq;
	using System.Linq.Expressions;
	using System.ComponentModel;
	using System;
	
	
	[global::System.Data.Linq.Mapping.DatabaseAttribute(Name="DHRDataWarehouse")]
	public partial class DHRDataWarehouseDataContext : System.Data.Linq.DataContext
	{
		
		private static System.Data.Linq.Mapping.MappingSource mappingSource = new AttributeMappingSource();
		
    #region Extensibility Method Definitions
    partial void OnCreated();
    partial void InsertWCCarrier(WCCarrier instance);
    partial void UpdateWCCarrier(WCCarrier instance);
    partial void DeleteWCCarrier(WCCarrier instance);
    partial void InsertWCRate(WCRate instance);
    partial void UpdateWCRate(WCRate instance);
    partial void DeleteWCRate(WCRate instance);
    partial void InsertWCState(WCState instance);
    partial void UpdateWCState(WCState instance);
    partial void DeleteWCState(WCState instance);
    partial void InsertSutaRate(SutaRate instance);
    partial void UpdateSutaRate(SutaRate instance);
    partial void DeleteSutaRate(SutaRate instance);
    #endregion
		
		public DHRDataWarehouseDataContext() : 
				base(global::System.Configuration.ConfigurationManager.ConnectionStrings["DHRDataWarehouseConnectionString"].ConnectionString, mappingSource)
		{
			OnCreated();
		}
		
		public DHRDataWarehouseDataContext(string connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public DHRDataWarehouseDataContext(System.Data.IDbConnection connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public DHRDataWarehouseDataContext(string connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public DHRDataWarehouseDataContext(System.Data.IDbConnection connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public System.Data.Linq.Table<WCCarrier> WCCarriers
		{
			get
			{
				return this.GetTable<WCCarrier>();
			}
		}
		
		public System.Data.Linq.Table<WCRate> WCRates
		{
			get
			{
				return this.GetTable<WCRate>();
			}
		}
		
		public System.Data.Linq.Table<WCState> WCStates
		{
			get
			{
				return this.GetTable<WCState>();
			}
		}
		
		public System.Data.Linq.Table<SutaRate> SutaRates
		{
			get
			{
				return this.GetTable<SutaRate>();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.WCCarriers")]
	public partial class WCCarrier : INotifyPropertyChanging, INotifyPropertyChanged
	{
		
		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
		
		private int _ID;
		
		private string _Name;
		
		private EntitySet<WCState> _WCStates;
		
    #region Extensibility Method Definitions
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnIDChanging(int value);
    partial void OnIDChanged();
    partial void OnNameChanging(string value);
    partial void OnNameChanged();
    #endregion
		
		public WCCarrier()
		{
			this._WCStates = new EntitySet<WCState>(new Action<WCState>(this.attach_WCStates), new Action<WCState>(this.detach_WCStates));
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_ID", AutoSync=AutoSync.OnInsert, DbType="Int NOT NULL IDENTITY", IsPrimaryKey=true, IsDbGenerated=true)]
		public int ID
		{
			get
			{
				return this._ID;
			}
			set
			{
				if ((this._ID != value))
				{
					this.OnIDChanging(value);
					this.SendPropertyChanging();
					this._ID = value;
					this.SendPropertyChanged("ID");
					this.OnIDChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Name", DbType="VarChar(50) NOT NULL", CanBeNull=false)]
		public string Name
		{
			get
			{
				return this._Name;
			}
			set
			{
				if ((this._Name != value))
				{
					this.OnNameChanging(value);
					this.SendPropertyChanging();
					this._Name = value;
					this.SendPropertyChanged("Name");
					this.OnNameChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.AssociationAttribute(Name="WCCarrier_WCState", Storage="_WCStates", ThisKey="ID", OtherKey="CarrierID")]
		public EntitySet<WCState> WCStates
		{
			get
			{
				return this._WCStates;
			}
			set
			{
				this._WCStates.Assign(value);
			}
		}
		
		public event PropertyChangingEventHandler PropertyChanging;
		
		public event PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			if ((this.PropertyChanging != null))
			{
				this.PropertyChanging(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(String propertyName)
		{
			if ((this.PropertyChanged != null))
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
		
		private void attach_WCStates(WCState entity)
		{
			this.SendPropertyChanging();
			entity.WCCarrier = this;
		}
		
		private void detach_WCStates(WCState entity)
		{
			this.SendPropertyChanging();
			entity.WCCarrier = null;
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.WCRates")]
	public partial class WCRate : INotifyPropertyChanging, INotifyPropertyChanged
	{
		
		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
		
		private int _ID;
		
		private int _StateID;
		
		private string _ClassCode;
		
		private double _FinalRate;
		
		private EntityRef<WCState> _WCState;
		
    #region Extensibility Method Definitions
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnIDChanging(int value);
    partial void OnIDChanged();
    partial void OnStateIDChanging(int value);
    partial void OnStateIDChanged();
    partial void OnClassCodeChanging(string value);
    partial void OnClassCodeChanged();
    partial void OnFinalRateChanging(double value);
    partial void OnFinalRateChanged();
    #endregion
		
		public WCRate()
		{
			this._WCState = default(EntityRef<WCState>);
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_ID", AutoSync=AutoSync.OnInsert, DbType="Int NOT NULL IDENTITY", IsPrimaryKey=true, IsDbGenerated=true)]
		public int ID
		{
			get
			{
				return this._ID;
			}
			set
			{
				if ((this._ID != value))
				{
					this.OnIDChanging(value);
					this.SendPropertyChanging();
					this._ID = value;
					this.SendPropertyChanged("ID");
					this.OnIDChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_StateID", DbType="Int NOT NULL")]
		public int StateID
		{
			get
			{
				return this._StateID;
			}
			set
			{
				if ((this._StateID != value))
				{
					if (this._WCState.HasLoadedOrAssignedValue)
					{
						throw new System.Data.Linq.ForeignKeyReferenceAlreadyHasValueException();
					}
					this.OnStateIDChanging(value);
					this.SendPropertyChanging();
					this._StateID = value;
					this.SendPropertyChanged("StateID");
					this.OnStateIDChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_ClassCode", DbType="VarChar(20) NOT NULL", CanBeNull=false)]
		public string ClassCode
		{
			get
			{
				return this._ClassCode;
			}
			set
			{
				if ((this._ClassCode != value))
				{
					this.OnClassCodeChanging(value);
					this.SendPropertyChanging();
					this._ClassCode = value;
					this.SendPropertyChanged("ClassCode");
					this.OnClassCodeChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_FinalRate", DbType="Float NOT NULL")]
		public double FinalRate
		{
			get
			{
				return this._FinalRate;
			}
			set
			{
				if ((this._FinalRate != value))
				{
					this.OnFinalRateChanging(value);
					this.SendPropertyChanging();
					this._FinalRate = value;
					this.SendPropertyChanged("FinalRate");
					this.OnFinalRateChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.AssociationAttribute(Name="WCState_WCRate", Storage="_WCState", ThisKey="StateID", OtherKey="ID", IsForeignKey=true)]
		public WCState WCState
		{
			get
			{
				return this._WCState.Entity;
			}
			set
			{
				WCState previousValue = this._WCState.Entity;
				if (((previousValue != value) 
							|| (this._WCState.HasLoadedOrAssignedValue == false)))
				{
					this.SendPropertyChanging();
					if ((previousValue != null))
					{
						this._WCState.Entity = null;
						previousValue.WCRates.Remove(this);
					}
					this._WCState.Entity = value;
					if ((value != null))
					{
						value.WCRates.Add(this);
						this._StateID = value.ID;
					}
					else
					{
						this._StateID = default(int);
					}
					this.SendPropertyChanged("WCState");
				}
			}
		}
		
		public event PropertyChangingEventHandler PropertyChanging;
		
		public event PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			if ((this.PropertyChanging != null))
			{
				this.PropertyChanging(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(String propertyName)
		{
			if ((this.PropertyChanged != null))
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.WCState")]
	public partial class WCState : INotifyPropertyChanging, INotifyPropertyChanged
	{
		
		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
		
		private int _ID;
		
		private System.DateTime _StartDate;
		
		private string _State;
		
		private int _CarrierID;
		
		private EntitySet<WCRate> _WCRates;
		
		private EntityRef<WCCarrier> _WCCarrier;
		
    #region Extensibility Method Definitions
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnIDChanging(int value);
    partial void OnIDChanged();
    partial void OnStartDateChanging(System.DateTime value);
    partial void OnStartDateChanged();
    partial void OnStateChanging(string value);
    partial void OnStateChanged();
    partial void OnCarrierIDChanging(int value);
    partial void OnCarrierIDChanged();
    #endregion
		
		public WCState()
		{
			this._WCRates = new EntitySet<WCRate>(new Action<WCRate>(this.attach_WCRates), new Action<WCRate>(this.detach_WCRates));
			this._WCCarrier = default(EntityRef<WCCarrier>);
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_ID", AutoSync=AutoSync.OnInsert, DbType="Int NOT NULL IDENTITY", IsPrimaryKey=true, IsDbGenerated=true)]
		public int ID
		{
			get
			{
				return this._ID;
			}
			set
			{
				if ((this._ID != value))
				{
					this.OnIDChanging(value);
					this.SendPropertyChanging();
					this._ID = value;
					this.SendPropertyChanged("ID");
					this.OnIDChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_StartDate", DbType="DateTime")]
		public System.DateTime StartDate
		{
			get
			{
				return this._StartDate;
			}
			set
			{
				if ((this._StartDate != value))
				{
					this.OnStartDateChanging(value);
					this.SendPropertyChanging();
					this._StartDate = value;
					this.SendPropertyChanged("StartDate");
					this.OnStartDateChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_State", DbType="Char(2)", CanBeNull=false)]
		public string State
		{
			get
			{
				return this._State;
			}
			set
			{
				if ((this._State != value))
				{
					this.OnStateChanging(value);
					this.SendPropertyChanging();
					this._State = value;
					this.SendPropertyChanged("State");
					this.OnStateChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_CarrierID", DbType="Int NOT NULL")]
		public int CarrierID
		{
			get
			{
				return this._CarrierID;
			}
			set
			{
				if ((this._CarrierID != value))
				{
					if (this._WCCarrier.HasLoadedOrAssignedValue)
					{
						throw new System.Data.Linq.ForeignKeyReferenceAlreadyHasValueException();
					}
					this.OnCarrierIDChanging(value);
					this.SendPropertyChanging();
					this._CarrierID = value;
					this.SendPropertyChanged("CarrierID");
					this.OnCarrierIDChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.AssociationAttribute(Name="WCState_WCRate", Storage="_WCRates", ThisKey="ID", OtherKey="StateID")]
		public EntitySet<WCRate> WCRates
		{
			get
			{
				return this._WCRates;
			}
			set
			{
				this._WCRates.Assign(value);
			}
		}
		
		[global::System.Data.Linq.Mapping.AssociationAttribute(Name="WCCarrier_WCState", Storage="_WCCarrier", ThisKey="CarrierID", OtherKey="ID", IsForeignKey=true)]
		public WCCarrier WCCarrier
		{
			get
			{
				return this._WCCarrier.Entity;
			}
			set
			{
				WCCarrier previousValue = this._WCCarrier.Entity;
				if (((previousValue != value) 
							|| (this._WCCarrier.HasLoadedOrAssignedValue == false)))
				{
					this.SendPropertyChanging();
					if ((previousValue != null))
					{
						this._WCCarrier.Entity = null;
						previousValue.WCStates.Remove(this);
					}
					this._WCCarrier.Entity = value;
					if ((value != null))
					{
						value.WCStates.Add(this);
						this._CarrierID = value.ID;
					}
					else
					{
						this._CarrierID = default(int);
					}
					this.SendPropertyChanged("WCCarrier");
				}
			}
		}
		
		public event PropertyChangingEventHandler PropertyChanging;
		
		public event PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			if ((this.PropertyChanging != null))
			{
				this.PropertyChanging(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(String propertyName)
		{
			if ((this.PropertyChanged != null))
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
		
		private void attach_WCRates(WCRate entity)
		{
			this.SendPropertyChanging();
			entity.WCState = this;
		}
		
		private void detach_WCRates(WCRate entity)
		{
			this.SendPropertyChanging();
			entity.WCState = null;
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.SutaRates")]
	public partial class SutaRate : INotifyPropertyChanging, INotifyPropertyChanged
	{
		
		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
		
		private string _State;
		
		private System.DateTime _Insert_dts;
		
		private double _SutaCost;
		
    #region Extensibility Method Definitions
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnStateChanging(string value);
    partial void OnStateChanged();
    partial void OnInsert_dtsChanging(System.DateTime value);
    partial void OnInsert_dtsChanged();
    partial void OnSutaCostChanging(double value);
    partial void OnSutaCostChanged();
    #endregion
		
		public SutaRate()
		{
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_State", DbType="Char(2) NOT NULL", CanBeNull=false, IsPrimaryKey=true)]
		public string State
		{
			get
			{
				return this._State;
			}
			set
			{
				if ((this._State != value))
				{
					this.OnStateChanging(value);
					this.SendPropertyChanging();
					this._State = value;
					this.SendPropertyChanged("State");
					this.OnStateChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Insert_dts", DbType="DateTime NOT NULL", IsPrimaryKey=true)]
		public System.DateTime Insert_dts
		{
			get
			{
				return this._Insert_dts;
			}
			set
			{
				if ((this._Insert_dts != value))
				{
					this.OnInsert_dtsChanging(value);
					this.SendPropertyChanging();
					this._Insert_dts = value;
					this.SendPropertyChanged("Insert_dts");
					this.OnInsert_dtsChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_SutaCost", DbType="Float NOT NULL")]
		public double SutaCost
		{
			get
			{
				return this._SutaCost;
			}
			set
			{
				if ((this._SutaCost != value))
				{
					this.OnSutaCostChanging(value);
					this.SendPropertyChanging();
					this._SutaCost = value;
					this.SendPropertyChanged("SutaCost");
					this.OnSutaCostChanged();
				}
			}
		}
		
		public event PropertyChangingEventHandler PropertyChanging;
		
		public event PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			if ((this.PropertyChanging != null))
			{
				this.PropertyChanging(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(String propertyName)
		{
			if ((this.PropertyChanged != null))
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
	}
}
#pragma warning restore 1591
