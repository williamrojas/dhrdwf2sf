﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi_DhrDwh2Sf.Models
{
    /// <summary>
    /// A Carrier Rates record
    /// </summary>
    public class CarrierRates
    {
        
        public int ID { get; set; }
        public string ClassCode { get; set; }
        public string State { get; set; }
        public double FinalRate { get; set; }
        public int CarrierId { get; set; }
        public DateTime? StartDate { get; set; }
    }
}